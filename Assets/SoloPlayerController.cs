﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoloPlayerController : MonoBehaviour
{

    public bool haveBall;
    private float speed = 16.0f;

    public float speedo = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;

    public float rotateSpeed = 3.0F;
    // Use this for initialization
    void Start()
    {
        haveBall = false;
    }

    // Update is called once per frame
    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float curSpeed = speedo * Input.GetAxis("Vertical");
        controller.SimpleMove(forward * curSpeed);

        /*if (!haveBall)
        {
            if(Input.GetKeyDown(KeyCode.A))
            {
                transform.position = new Vector3(transform.position.x * speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                transform.position = new Vector3(transform.position.x * -speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            if (Input.GetKeyDown(KeyCode.W))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z * speed * Time.deltaTime);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z * -speed * Time.deltaTime);
            }
        }*/
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Ball")
        {
            haveBall = true;
            other.gameObject.transform.SetParent(this.gameObject.transform);
            other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
