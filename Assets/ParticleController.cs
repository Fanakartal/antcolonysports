﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public float minVelocity = 5;
    public float maxVelocity = 20;
    public float randomness = 1;
    public int flockSize = 10;
    //public GameObject prefab;
    public GameObject chasee;

    public Vector3 flockCenter;
    public Vector3 flockVelocity;

    public GameObject[] swarm;

    void Start()
    {
        //swarm = new GameObject[flockSize];
        for (int i = 0; i < flockSize; i++)
        {
            /*Vector3 position = new Vector3(
                Random.value * this.gameObject.GetComponent<Collider>().bounds.size.x,
                Random.value * this.gameObject.GetComponent<Collider>().bounds.size.y,
                Random.value * this.gameObject.GetComponent<Collider>().bounds.size.z
            ) - this.gameObject.GetComponent<Collider>().bounds.extents;*/

            //GameObject bee = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
            //swarm[i].transform.parent = transform;
            //swarm[i].transform.localPosition = position;
            swarm[i].GetComponent<BeeFlocking>().SetController(gameObject);
            //swarm[i] = bee;
        }
    }

    void Update()
    {
        Vector3 theCenter = Vector3.zero;
        Vector3 theVelocity = Vector3.zero;

        foreach (GameObject bee in swarm)
        {
            theCenter = theCenter + bee.transform.localPosition;// new Vector3(bee.transform.localPosition.x, 0.0f, bee.transform.localPosition.z);
            theVelocity = theVelocity + new Vector3(bee.GetComponent<Rigidbody>().velocity.x, 0.0f, bee.GetComponent<Rigidbody>().velocity.z);
        }

        flockCenter = theCenter / (flockSize);
        flockVelocity = theVelocity / (flockSize);
    }
}
